#!/usr/bin/env bash

# set build release version in ENV to propagate it in documents
export BUILD_RELEASE=`cat ./VERSION`

# required to avoid pdf make to fail if a french latex build was made before
rm -r build/latex

# HTML
# use tag for only directives (adapt image sizes for output)
make html SPHINXOPTS="-t html -D version='${BUILD_RELEASE}' -D release='${BUILD_RELEASE}' -D language='fr_FR'"

# EPUB
make epub SPHINXOPTS="-t epub -D version='${BUILD_RELEASE}' -D release='${BUILD_RELEASE}' -D language='fr_FR' -D epub_title='Théorie Relative de la Monnaie v2.718 - rev. ${BUILD_RELEASE}'"
cp build/epub/TheorieRelativedelaMonnaie.epub build/html/.

# PDF
make latexpdf SPHINXOPTS="-t latex -D version='${BUILD_RELEASE}' -D release='${BUILD_RELEASE}' -D language='fr_FR'"
cp build/latex/TheorieRelativedelaMonnaie.pdf build/html/.

# check build
python ./bin/build_check.py
status=$?
[ $status -eq 0 ] && echo "" || exit 1

# label folder as french version
rm -rf build/fr_FR
mv build/html build/fr_FR
